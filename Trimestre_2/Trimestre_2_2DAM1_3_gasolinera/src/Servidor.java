import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor implements Runnable{
	
	// SOCKET TCP
	// ArrayList con los clientes conectados a nuestro servidor
	private ArrayList<Socket> clientes;
	
	private int puerto;
	
	public Servidor(int puerto) {
		super();
		this.puerto = puerto;
		this.clientes = new ArrayList();
	}

	@Override
	public void run() {
		ServerSocket servidor = null;
		Socket cliente = null;
		
		try {
			servidor = new ServerSocket(puerto);
			
			// El servidor escuchando peticiones
			while(true) {
				//Acepto peticion del cliente
				cliente = servidor.accept();
				
				//Agregar cliente al ArrayList
				clientes.add(cliente);
			}
		}catch(Exception e) {}
	}
	
	// Recibir como parametros nombres y valores de la gasolina y enviarlos a los clientes.
	public void enviarInfo(String[] nombres, double[] valores) {
		for(Socket cliente: clientes) {
			try {
				DataOutputStream out = new DataOutputStream(cliente.getOutputStream());
				for(int i = 0; i < nombres.length; i++) {
					out.writeUTF(nombres[i]);
					out.writeDouble(valores[i]);
				}
			} catch (IOException e) {}
		}
	}
}
