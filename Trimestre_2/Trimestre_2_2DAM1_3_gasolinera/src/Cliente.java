import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Observable;

@SuppressWarnings("deprecation")
public class Cliente extends Observable implements Runnable {
	
	private int puerto;
	
	
	public Cliente(int puerto) {
		super();
		this.puerto = puerto;
	}

	@Override
	public void run() {
		//HOST del servidor
		final String HOST = "127.0.0.1";
		
		DataInputStream in;
		
		try {
			// Crear el socket para conectarme
			Socket cliente = new Socket(HOST, puerto);
			in = new DataInputStream(cliente.getInputStream());
			
			String nombre;
			double valor;
			
			// Notificar cada vez que me lleguen datos por flujo de entrada (en este caso un string nombre y un double valor)
			while(true) {
				nombre = in.readUTF();
				
				this.setChanged();
				this.notifyObservers(nombre);
				this.clearChanged();
				
				valor = in.readDouble();
				
				this.setChanged();
				this.notifyObservers(valor);
				this.clearChanged();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
