import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor_TCP {
	public static void main (String[] args) {
	
		DataInputStream in;
		DataOutputStream out;
		
		final int PUERTO = 1000;
		
		try {
			ServerSocket servidor = new ServerSocket(PUERTO);
			//servidor.setSoTimeout(5000);
			System.out.println("El servidor esta creado");
			
				
			while(true) {
				Socket cliente = servidor.accept();
				in = new DataInputStream(cliente.getInputStream());
				out = new DataOutputStream(cliente.getOutputStream());
				// Thread.sleep(6000);
				System.out.println("El cliente esta conectado");
				
				String mensaje = in.readUTF();
				
				System.out.println("Mensaje recibido: " +mensaje);
				
				out.writeUTF("Hola soy el SERVIDOR");
				
				cliente.close(); // Cierro el cliente
				
				System.out.println("El cliente esta desconectado");
				
			}
			
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Socket servidor
	}
}
