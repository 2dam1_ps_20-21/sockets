import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Cliente_TCP {
	
	public static void main(String[] args) {
		final String HOST = "192.168.1.136"; //Hace referencia a mi propia maquina
		final int PUERTO = 1000; // Asigno el mismo puerto que en el servidor
		
		DataInputStream in;
		DataOutputStream out;
		try {
			Socket soc = new Socket(HOST, PUERTO); // Creacion del socket cliente
			// soc.setSoTimeout(5000);
			in = new DataInputStream(soc.getInputStream());
			out = new DataOutputStream(soc.getOutputStream());
			
			out.writeUTF("Hola soy el cliente"); // enviar mensaje al servidor 
			
			String mensaje = in.readUTF(); // recepcion del mensaje del servidor 
			
			System.out.println(mensaje);
			
			soc.close();
		} 
		catch (UnknownHostException e) {} 
		catch (IOException e) {}
		/*catch (SocketException e) {
			
		}*/
	}
}
