import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastClient {
	public static void main (String[] args) {
		
		try {
			byte[] buffer = new byte[1024];
			DatagramPacket dgram = new DatagramPacket(buffer, buffer.length);
			MulticastSocket socket = new MulticastSocket(4000);
			
			InetAddress group  = InetAddress.getByName("235.1.1.1");
			socket.joinGroup(group);
			
			while(true) {
				// Esta bloqueado hasta recibir datagramas
				socket.receive(dgram); 
				System.out.println("Recibido mensaje de longitud: " + dgram.getLength() + " bytes.");
            	System.out.println("Direccion de llegada: " + dgram.getAddress());
            	System.out.println("Contenido del mensaje: " + new String(dgram.getData()));
            	System.out.println("--------------------------------------------------------");
            	//socket.leaveGroup(group);
			}
			
		} catch (IOException e) {}
	}
}
