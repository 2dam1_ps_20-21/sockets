import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastServer {
	public static void main (String[] args) {
		try {
			MulticastSocket socket = new MulticastSocket();
			byte[] buffer = "Mensaje".getBytes();
			DatagramPacket dgram;
			
			int PUERTO = 4000;
			
	        dgram = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("235.1.1.1"), PUERTO);
	        
	        System.out.println("Enviando mensaje de longitud: " + dgram.getLength() + " bytes.");
        	System.out.println("Direccion de envio: " + dgram.getAddress() + ':' + dgram.getPort());
        	System.out.println("--------------------------------------------------------");

        	int contador = 0;
	        while(true) {
	        	contador ++;
	        	System.out.println("Envio mensaje " + contador);
	        	socket.send(dgram);
	        	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
	        }
		} catch (IOException e) {}
	}
}
