import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Usuario2 {
	public static void main(String[] args) {
		final int PUERTO = 300;
		byte[] buffer = new byte[1024];
		
		try {
			/* RECEPCION */
			//Creo el Datagrama
			DatagramSocket socketUDP = new DatagramSocket(PUERTO);
			
			//Preparo la recepcion
			DatagramPacket recepcion = new DatagramPacket(buffer, buffer.length);
			socketUDP.receive(recepcion);
			
			String mensaje = new String(recepcion.getData());
			System.out.println("Usuario 2 recibe el siguiente mensaje: " + mensaje);
			
			/* ENVIO */
			//Obtengo puerto y direccion
			int puertoUsuario1 = recepcion.getPort();
			InetAddress direccion = recepcion.getAddress();
			
			//Preparo el mensaje
			mensaje = "Hola desde el usuario2";
			buffer = mensaje.getBytes();
			
			//Preparar la respuesta
			DatagramPacket respuesta = new DatagramPacket(buffer, buffer.length, direccion, puertoUsuario1);
			socketUDP.send(respuesta);
			
			socketUDP.close();
		}catch(Exception e) {
			
		}
	}
}
