import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Usuario1 {
	public static void main(String[] args) {
		final int PUERTO = 300;
		byte[] buffer = new byte[1024];
		
		try {
			//Direccion
			InetAddress direccion = InetAddress.getByName("localhost");
			
			//Creo el Datagrama
			DatagramSocket socketUDP = new DatagramSocket();
			
			/* ENVIO */
			//Preparar el mensaje
			String mensaje = "Hola desde el Usuario 1";
			buffer = mensaje.getBytes();
			
			//Preparar el envio
			DatagramPacket envio = new DatagramPacket(buffer, buffer.length, direccion, PUERTO);
			socketUDP.send(envio);
			
			/* RECEPCION */
			//Preparo la recepcion
			DatagramPacket recepcion = new DatagramPacket(buffer, buffer.length);
			socketUDP.receive(recepcion);
			
			//Obtengo el mensaje
			mensaje = new String(recepcion.getData());
			System.out.println("Usuario 1 recibe el siguiente mensaje: " + mensaje);
			
			socketUDP.close();
		}catch(Exception e) {
			
		}
	}
}
